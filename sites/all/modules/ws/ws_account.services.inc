<?php
function ws_account_services_resources() {
  $resources = array(
  		'learning' => array(
  				'actions' => array(
  				    'get_grades' => array (
  				        'file' => array (
  				            'type' => 'inc',
  				            'module' => 'ws_account',
  				            'name' => 'ws_account.resource'
  				        ),
  				        'callback' => 'get_grades',
  				        'args' => array (
  				            array (
  				                'name' => 'data',
  				                'type' => 'struct',
  				                'description' => 'The note object',
  				                'source' => 'data',
  				                'optional' => FALSE
  				            )
  				        ),
  				        'access callback' => '_ws_account_services_access'
  				    ),
  				    'get_subjects' => array (
  				        'file' => array (
  				            'type' => 'inc',
  				            'module' => 'ws_account',
  				            'name' => 'ws_account.resource'
  				        ),
  				        'callback' => 'get_subjects',
  				        'args' => array (
  				            array (
  				                'name' => 'data',
  				                'type' => 'struct',
  				                'description' => 'The note object',
  				                'source' => 'data',
  				                'optional' => FALSE
  				            )
  				        ),
  				        'access callback' => '_ws_account_services_access'
  				    ),
  				    'get_chapters' => array (
  				        'file' => array (
  				            'type' => 'inc',
  				            'module' => 'ws_account',
  				            'name' => 'ws_account.resource'
  				        ),
  				        'callback' => 'get_chapters',
  				        'args' => array (
  				            array (
  				                'name' => 'data',
  				                'type' => 'struct',
  				                'description' => 'The note object',
  				                'source' => 'data',
  				                'optional' => FALSE
  				            )
  				        ),
  				        'access callback' => '_ws_account_services_access'
  				    ),
  				    'get_topics' => array (
  				        'file' => array (
  				            'type' => 'inc',
  				            'module' => 'ws_account',
  				            'name' => 'ws_account.resource'
  				        ),
  				        'callback' => 'get_topics',
  				        'args' => array (
  				            array (
  				                'name' => 'data',
  				                'type' => 'struct',
  				                'description' => 'The note object',
  				                'source' => 'data',
  				                'optional' => FALSE
  				            )
  				        ),
  				        'access callback' => '_ws_account_services_access'
  				    ),
  				    'topic_detail' => array (
  				        'file' => array (
  				            'type' => 'inc',
  				            'module' => 'ws_account',
  				            'name' => 'ws_account.resource'
  				        ),
  				        'callback' => 'topic_detail',
  				        'args' => array (
  				            array (
  				                'name' => 'data',
  				                'type' => 'struct',
  				                'description' => 'The note object',
  				                'source' => 'data',
  				                'optional' => FALSE
  				            )
  				        ),
  				        'access callback' => '_ws_account_services_access'
  				    ),
  				    'question_detail' => array (
  				        'file' => array (
  				            'type' => 'inc',
  				            'module' => 'ws_account',
  				            'name' => 'ws_account.resource'
  				        ),
  				        'callback' => 'question_detail',
  				        'args' => array (
  				            array (
  				                'name' => 'data',
  				                'type' => 'struct',
  				                'description' => 'The note object',
  				                'source' => 'data',
  				                'optional' => FALSE
  				            )
  				        ),
  				        'access callback' => '_ws_account_services_access'
  				    ),
  				),  				
  		'retrieve' => array(
  				'file' => array('type' => 'inc', 'module' => 'ws_account', 'name' => 'ws_account.resource'),
  				'callback' => '_ws_account_retrieve',
  				'args' => array(
  						array(
  								'name' => 'arg1',
  								'optional' => FALSE,
  								'source' => array('path' => 0),
  								'type' => 'int',
  								'description' => 'Path of the call',
  						),
      				    array(
      				        'name' => 'unique_id',
      				        'optional' => TRUE,
      				        'source' => array('param' => 'unique_id'),
      				        'type' => 'string',
      				        'description' => 'Unique Id of the question paper',
      				    ),
      				    array(
      				        'name' => 'board',
      				        'optional' => TRUE,
      				        'source' => array('param' => 'board'),
      				        'type' => 'string',
      				        'description' => 'Board',
      				    ),
      				    array(
      				        'name' => 'class',
      				        'optional' => TRUE,
      				        'source' => array('param' => 'class'),
      				        'type' => 'string',
      				        'description' => 'Class',
      				    ),
      				    array(
      				        'name' => 'subject',
      				        'optional' => TRUE,
      				        'source' => array('param' => 'subject'),
      				        'type' => 'string',
      				        'description' => 'Subject',
      				    ),
      				    array(
      				        'name' => 'question_id',
      				        'optional' => TRUE,
      				        'source' => array('param' => 'question_id'),
      				        'type' => 'string',
      				        'description' => 'Question Id',
      				    ),
      				    array(
      				        'name' => 'solution_id',
      				        'optional' => TRUE,
      				        'source' => array('param' => 'solution_id'),
      				        'type' => 'string',
      				        'description' => 'Solution Id',
      				    ),
        			    array(
        			        'name' => 'map_chapterId',
        			        'optional' => TRUE,
        			        'source' => array('param' => 'map_chapterId'),
        			        'type' => 'string',
        			        'description' => 'Solution Map Chapter Id',
        			    ),
      				    array(
      				        'name' => 'slug',
      				        'optional' => TRUE,
      				        'source' => array('param' => 'slug'),
      				        'type' => 'string',
      				        'description' => 'Seo Slug',
      				    )
  				    
  				),
  				'access callback' => '_ws_account_services_access',
  		),
  	),			
  		
  );
  return $resources;
}


/**
 * Access callback.
 */
function _ws_account_services_access($op) {
	
  //TODO explore the access based on user permission
  //return user_access($op);
  return TRUE;
}


function _ws_account_retrieve($op, $unique_id, $board, $class, $subject, $question_id, $solution_id, $map_chapterId, $slug) {	
  $output = array();
  
  if($op == 'question_paper'){
      if(empty($unique_id)){
    		return services_error('Missing data [unique_id]', 406);
      }
      else{
          $questionPaper = ws_get_question_paper($unique_id);
          $other_questionPapers = get_other_question_papers($unique_id);
//          $page_type, $entity_id, $fk_boardId=NULL, $fk_gradeId=NULL
          $question_id = ($questionPaper)?$questionPaper['id']:NULL;
          $seo_data = get_seo_data(PAGE_TYPE_QUESTION_PAPER, $question_id, NULL, NULL);
          $data_array['questionPaper'] = $questionPaper;
          $data_array['other_questionPaper'] = $other_questionPapers;
          $data_array['seo_data'] = $seo_data;
         
          $output['status'] = 200;
          $output['message'] = "Success";
          $output['data'] = $data_array;
      }
  }
  
  if($op == 'get_boards'){
      $boards = ws_get_school_boards();
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $boards;
  }

  if($op == 'get_headers'){
      $headers = ws_get_headers();
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $headers;
  }
  
  if($op == 'home'){
      $headers = ws_home();
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $headers;
  }
  
  if($op == 'board_class_subject'){
      $data = ws_board_class_subjects($board, $class, $subject);
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $data;
  }

  if($op == 'get_solution_data'){
      $data = ws_get_solution_data($slug);
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $data;
  }

  if($op == 'get_solution_question'){
      $data = ws_get_solution_chapter_data($slug);
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $data;
  }
  
  if($op == 'create_seo'){
      $data = ws_create_seo();
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $data;
  }
  
  if($op == 'create_qp_seo'){ //create question paper seo
      $data = ws_create_qp_seo();
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $data;
  }
  
  if($op == 'create_solution_seo'){
      $data = create_all_solutions_seo();
      $output['status'] = 200;
      $output['message'] = "Success";
      $output['data'] = $data;
  }
  
  return $output;
}

function ws_create_seo(){
    $board_seo = NULL;
    //Create board seo
    $current_active_academic_session = db_query("Select * from school_academic_year where status='active' AND current_session='yes' ")->fetchObject();
    if($current_active_academic_session && sizeof($current_active_academic_session)>0){
        $board_seo = create_all_board_seo($current_active_academic_session);
    }
    return $board_seo;
}

function ws_create_qp_seo(){
    //Create all question paper seo
    $board_seo = create_all_qp_seo();
    return $board_seo;
}

function ws_create_solutions_seo(){
    $solutions_seo = 0;
    $solutions_seo = create_all_solutions_seo();
    return $solutions_seo;
}

function get_grades($data) {
    $output = array();
    if (empty ( $data ['board'] ))
        return services_error ( 'Missing data [board]', 406 );
    else {
        $data_array = ws_board_grades_page( $data );
        $output['status'] = 200;
        $output['message'] = "Success";
        $output['data'] = ($data_array);        
        return ( object ) $output;
    }
}

function get_subjects($data){
    
    
    if (empty ( $data ['board'] )){
        return services_error ( 'Missing data [board]', 406 );
    }
    else if(empty($data['grade'])){
        return services_error ( 'Missing data [grade]', 406 );
    }
    else {
        
        $current_active_academic_session = db_query("Select * from school_academic_year where status='active' AND current_session='yes' ")->fetchObject();
        $data['academicYearId'] = $current_active_academic_session->id;
        
        $data_array = array();
        $data_array['title'] = ucwords($data['board'])." Class-".$data['grade'];
        $data_array['subjects'] = ws_get_grade_subjects( $data );
        $data_array['other_grades'] = get_board_other_grades($data);
        
        $gradeInfos = db_query("Select * from school_grade where status='active' AND grade LIKE :grade", array(':grade' => db_like($data['grade'])))->fetchAll();
        $gradeInfo = $gradeInfos[0];
        $gradeId = ($gradeInfo)?$gradeInfo->id:0;
        
        $data_array['seo_data'] = get_seo_data(PAGE_TYPE_GRADE, $gradeId, $current_active_academic_session->id);
       
        $output['status'] = 200;
        $output['message'] = "Success";
        $output['data'] = $data_array;
        return ( object ) $output;
    }   
}

function get_chapters($data){
    if (empty ( $data ['board'] )){
        return services_error ( 'Missing data [board]', 406 );
    }
    else if(empty($data['grade'])){
        return services_error ( 'Missing data [grade]', 406 );
    }
    else if(empty($data['subject'])){
        return services_error ( 'Missing data [subject]', 406 );
    }
    else {
        $data_array = array();
        if( isset($data['year']) || (!empty($data['year'])) ){
            $current_active_academic_session = db_query("Select * from school_academic_year where status='active' AND current_session='yes' ")->fetchObject();
            $data['academicYearId'] = $current_active_academic_session->id;
        }else{
            $current_active_academic_session = db_query("Select * from school_academic_year where status='active' AND current_session='yes' ")->fetchObject();
            $data['academicYearId'] = $current_active_academic_session->id;
        }
        
        $data_array['title'] = ucwords($data['board'])." Class-".$data['grade']." ".ucwords($data['subject']);
        $data_array['chapters'] = ws_get_subject_chapters( $data );
        $data_array['other_subjects'] = get_grade_other_subjects($data);
        $data_array['question_papers'] = get_chapter_questions($data);
        
        $boardInfos = db_query("Select * from school_boards where status='active' AND board_name LIKE :board_name", array(':board_name' => db_like($data['board'])))->fetchAll();
        $boardInfo = $boardInfos[0];
        $boardId = ($boardInfo)?$boardInfo->id:0;
        
        $gradeId = db_query("Select id from school_grade where status='active' AND grade LIKE :grade", array(':grade' => db_like($data['grade'])))->fetchField();
        $subjectId = db_query("Select id from school_subjects where status='active' AND subject LIKE :subject", array(':subject' => db_like($data['subject'])))->fetchField();
        
        
        $data_array['seo_data'] = get_seo_data('S', $subjectId, $current_active_academic_session->id, $boardId, $gradeId);
        
        $output['status'] = 200;
        $output['message'] = "Success";
        $output['data'] = $data_array;
        
        return ( object ) $output;
    }
}

function get_topics($data){
    if (empty ( $data ['board'] )){
        return services_error ( 'Missing data [board]', 406 );
    }
    else if(empty($data['grade'])){
        return services_error ( 'Missing data [grade]', 406 );
    }
    else if(empty($data['subject'])){
        return services_error ( 'Missing data [subject]', 406 );
    }
    else if(empty($data['chapter'])){
        return services_error ( 'Missing data [chapter]', 406 );
    }
    else {
        if(empty($data['year'])){
            $current_active_academic_session = db_query("Select * from school_academic_year where status='active' AND current_session='yes' ")->fetchObject();
            $data['academicYearId'] = $current_active_academic_session->id;
        }else{
            $current_active_academic_session = db_query("Select * from school_academic_year where status='active' AND current_session='yes' ")->fetchObject();
            $data['academicYearId'] = $current_active_academic_session->id;
        }
        
        
        $chapter_data = db_query("Select * from school_chapters where status='active' AND chapterName LIKE :chapterName", array(':chapterName' => db_like($data['chapter'])))->fetchObject();
        
        $title = ucwords($data['board'])." Class ".$data['grade']."-".ucwords($data['subject'])." Chapter:".ucwords($data['chapter']);
       
        $topics = ws_get_chapter_topics($data);
        $chapters = ws_get_subject_chapters($data);
        $data_array = array(
            'content'=> 'Chapter Topics',
            'chapter' => ucwords($data['chapter']),
            'chapter_desc'=> $chapter_data->chapter_desc,
            'board'=> $data['board'], 
            'grade'=> $data['grade'], 
            'subject'=>$data['subject'],
            'title'=>$title,
            'topics' =>$topics,
            'other_chapters' => $chapters,
            'breadcrumbs' => []
        );
        
        $output['status'] = 200;
        $output['message'] = "Success";
        $output['data'] = $data_array;
        return ( object ) $output;
    }
}

function ws_get_headers(){
    $data_array = [];
    $boards = ws_get_school_boards();
    $solutions = ws_get_school_solutions();
    
    $data['board'] = 'CBSE';
    
    $menus = [];
    $menus['board'] = $boards;
    
    $data_array['menu'] = array(
        'board' => $boards,
        'solutions' => $solutions,
        'blog' => null
    );
    return $data_array;
}

function ws_home(){
    $academicYearData = db_query("Select * from school_academic_year where status='active' AND current_session='yes' ")->fetchObject();
    $academicActiveYearId = $academicYearData->id;
    
    $boardId = db_query("Select entity_id from seo where academicYearId=:academicYearId and page_type='B' and status='publish' limit 1", array(':academicYearId'=>$academicActiveYearId))->fetchField();
    $gradeId = db_query("Select entity_id from seo where academicYearId=:academicYearId and page_type='G' and status='publish' limit 1", array(':academicYearId'=>$academicActiveYearId))->fetchField();
    $data = array(
        'academicYearId'=>$academicActiveYearId,
        'boardId'=>$boardId, 
        'gradeId'=>$gradeId,
    );
    $output = ws_get_home($data);
    return $output;
}

function ws_board_class_subjects($board, $class, $subject){
    $result = array();
    $data['board'] = $board;
    $data['grade'] = $class;
    $data['subject'] = $subject;
    $chapters = ws_get_subject_chapters($data);
    
    $title = ucwords($board)." Class ".$class."-".ucwords($subject);
    $result['title'] = ($title);
    $result['chapters'] = $chapters;
    $result['subjects'] = ws_get_grade_subjects($data);
    
    return $result;
}


function topic_detail($data){
    if (empty ( $data ['board'] )){
        return services_error ( 'Missing data [board]', 406 );
    }
    else if(empty($data['grade'])){
        return services_error ( 'Missing data [grade]', 406 );
    }
    else if(empty($data['subject'])){
        return services_error ( 'Missing data [subject]', 406 );
    }
    else if(empty($data['chapter'])){
        return services_error ( 'Missing data [chapter]', 406 );
    }
    else if(empty($data['topic'])){
        return services_error ( 'Missing data [topic]', 406 );
    }
    else {
        
        $academicYearData = db_query("Select * from school_academic_year where status='active' AND current_session='yes' ")->fetchObject();
        $academicActiveYearId = $academicYearData->id;
        $data['academicYearId'] = $academicActiveYearId;
        
        $title = ucwords($data['board'])." Class ".$data['grade']."-".ucwords($data['subject'])." Chapter:".ucwords($data['chapter']);
        
        $topics = ws_get_topic_detail($data);
        $other_topics = ws_get_chapter_topics($data);
        $data_array = array(
            'board'=> $data['board'],
            'grade'=> $data['grade'],
            'subject'=>$data['subject'],
            'chapter' => ucwords($data['chapter']),
            'title'=>$title,
            'topic'=>$data['topic'],
            'topic_desc'=>$topics['topic_desc'],
            'topics' =>$topics['topic_questions'],
            'other_topics' => $other_topics,
            'seo_data'=>$topics['seo_data']
        );
        $output['status'] = 200;
        $output['message'] = "Success";
        $output['data'] = $data_array;
        return ( object ) $output;
    }
}

function ws_get_topic_detail($data){
    
    $board = str_replace("-", " ", $data['board']);
    $grade = str_replace("-", " ", $data['grade']);
    $subject = str_replace("-", " ", $data['subject']);
    $chapter = str_replace("-", " ", $data['chapter']);
    $topic = str_replace("-", " ", $data['topic']);
    
    $boardId = db_query("Select id from school_boards where status='active' AND board_name=:board_name", array(':board_name' =>$board))->fetchField();
    $gradeId = db_query("Select id from school_grade where status='active' AND grade LIKE :grade", array(':grade' => db_like($grade)))->fetchField();
    $subjectId = db_query("Select id from school_subjects where status='active' AND subject LIKE :subject", array(':subject' => db_like($subject)))->fetchField();
    $chapterId = db_query("Select id from school_chapters where status='active' AND chapterName LIKE :chapterName", array(':chapterName' => db_like($chapter)))->fetchField();
    
    $academicSessionId = db_query("select c.id from school_academic_chapters as c inner join school_academic_year as y on y.id=c.fk_academicYearId where y.status='active' AND c.status='active' AND c.fk_boardId=:fk_boardId AND c.fk_gradeId=:fk_gradeId AND c.fk_subjectId=:fk_subjectId order by y.start_year desc, y.end_year desc limit 1 ", array(':fk_subjectId'=>$subjectId, ':fk_boardId'=>$boardId, ':fk_gradeId'=>$gradeId))->fetchField();    
    $topic_data = db_query("Select * from school_chapter_topics where topic LIKE :topic AND fk_chapterId=:fk_chapterId AND status='active'",array(':topic'=>$topic, ':fk_chapterId'=>$chapterId))->fetchObject();
    $topicId = $topic_data->id;
    $topic_questions = db_query("Select * from school_questions where status='active' AND fk_academicChapterId=:fk_academicChapterId AND fk_chapterId=:fk_chapterId AND fk_topicId=:fk_topicId", array(':fk_academicChapterId'=>$academicSessionId, ':fk_chapterId'=>$chapterId, ':fk_topicId'=>$topicId))->fetchAll();
    
    $data_array = array();
    $seo_data = get_seo_data('C', $chapterId, $data['academicYearId'], $boardId, $gradeId);
    $data_array['topic_questions'] = $topic_questions;
    $data_array['seo_data'] = $seo_data;
    $data_array['topic_desc'] = $topic_data->topic_desc;
    
    return $data_array;
}

function question_detail($data){
    //No SEO in question detail page
    $data_array = array();
    if (empty ( $data['question_id'] )){
        return services_error ( 'Missing data [question_id]', 406 );
    }
    else{
        $question_id = $data['question_id'];
        $result = db_select('school_questions','q')
        ->fields('q')
        ->condition('status','active')
        ->condition('id',$question_id,'=')
        ->execute()->fetchAll();
        
        $data_array = $data;
        $data_array['question'] = $result;
        $data_array['other_questions'] = get_other_questions($result[0]->fk_academicChapterId, $result[0]->fk_chapterId, $result[0]->fk_topicId);
        
        $output['status'] = 200;
        $output['message'] = "Success";
        $output['data'] = $data_array;
        return ( object ) $output;
        
    }
}

function get_other_questions($academicChapterId, $chapterId, $topicId){
    $result = db_select('school_questions','q')
    ->fields('q')
    ->condition('status','active')
    ->condition('fk_academicChapterId',$academicChapterId,'=')
    ->condition('fk_chapterId',$chapterId,'=')
    ->condition('fk_topicId',$topicId,'=')
    ->execute()->fetchAll();
    
    return $result;
}


function ws_get_solution_data($slug){
    $solutionName = null;
    $page_seo_data = db_query("Select * from seo where page_type='SLN' AND status='publish' AND seo_source=:seo_source",array(':seo_source'=>$slug))->fetchObject();
    $id = $page_seo_data->entity_id;
    
    $data_array = array();
    $query = db_select('school_grade_subject_solutions','t')
    ->fields('t')
    ->condition('t.status','active')
    ->condition('t.id',$id);
    $result = $query->execute()->fetchAll();
    if($result){
        foreach ($result as $res){
            $solutionName = get_data_by_pks('school_solutions', $res->fk_solutionId)->name;
            $query = db_select('school_solution_map_chapters','t')
            ->fields('t')
            ->condition('t.status','active')
            ->condition('t.fk_mapSolutionId',$res->id);
            $all_chapters = $query->execute()->fetchAll();
            $chapter_array = array();
            if($all_chapters){
                foreach($all_chapters as $chapter){
                    $chapterName = get_data_by_pks('school_chapters', $chapter->fk_chapterId)->chapterName;
                    $seo_data = db_query("Select * from seo where page_type='SLN_C' AND status='publish' AND entity_id=:entity_id",array(':entity_id'=>$chapter->id))->fetchObject();
                    $chapter_array[] = array(
                        'chapter'=>$chapterName,
                        'seo_data'=>$seo_data,
                        'seo_source'=>$seo_data->seo_source
                    );
                }
            }
            $data_array[] = array(
                'solutionName'=>$solutionName,
                'grade' => get_data_by_pks('school_grade', $res->fk_gradeId)->grade,
                'subject' => get_data_by_pks('school_subjects', $res->fk_subjectId)->subject,                
                'chapters'=>$chapter_array,
                'seo_data'=>$page_seo_data
            );
        }
    }
    return $data_array;
}

function ws_get_solution_chapter_data($slug){
    $data_array = array();    
    $page_seo_data = db_query("Select * from seo where page_type='SLN_C' AND status='publish' AND seo_source=:seo_source",array(':seo_source'=>$slug))->fetchObject();
    $mapId = $page_seo_data->entity_id;
    
    $query = db_select('school_solution_map_chapters','t')
    ->fields('t')
    ->condition('t.status','active')
    ->condition('t.id',$mapId);
    $all_chapters = $query->execute()->fetchAll();
    if($all_chapters){
        foreach($all_chapters as $chapter){
            $solutionMapData = get_data_by_pks('school_grade_subject_solutions', $chapter->fk_mapSolutionId);
            $solutionName = get_data_by_pks('school_solutions', $solutionMapData->fk_solutionId)->name;
            $chapterName = get_data_by_pks('school_chapters', $chapter->fk_chapterId)->chapterName;
            $questionAndAnswers = prepareSolutionsQuestionAnswers($chapter->fk_mapSolutionId, $chapter->fk_chapterId);
            $data_array[] = array(
                'solutionName' => $solutionName,
                'grade' => get_data_by_pks('school_grade', $solutionMapData->fk_gradeId)->grade,
                'subject' => get_data_by_pks('school_subjects', $solutionMapData->fk_subjectId)->subject,
                'chapters'=>$chapterName,
                'questions'=>$questionAndAnswers,
                'seo_data'=>$page_seo_data
            );
        }
    }
    
    return $data_array;
}

function prepareSolutionsQuestionAnswers($mapSolutionId, $chapterId){
    $all_questions = array();
    $quesData = db_select('school_solution_questions','tn')
    ->fields('tn')
    ->condition('status','active')
    ->condition('fk_solutionMapId',$mapSolutionId)
    ->condition('fk_chapterId',$chapterId)
    ->execute()->fetchAll();
    if($quesData){
        foreach($quesData as $quesData){
            $questionDetail = array();
            $questionDetail['question'] = $quesData->question;
            $questionDetail['answer'] = $quesData->answer;
            $questionDetail['order'] = $quesData->ordering;
            if($quesData->type == QUESTION_TYPE_FILL_BLANK){ $type = 'Fill in the blanks';}
            else if($quesData->type == QUESTION_TYPE_MCQ){ $type = 'MCQ';}
            else{$type = "Normal QnA";}
            $questionDetail['type'] = $type;
            if($type == QUESTION_TYPE_MCQ){
                $options = prepare_solution_question_options($eachQuestion->fk_questionId);
                $questionDetail['options'] = $options;
            } 
            $all_questions[] = $questionDetail;
        }
    }
    return $all_questions;
}

function prepare_solution_question_options($questionId){
    $options = db_select('school_solution_question_options','tn')
    ->fields('tn')
    ->condition('status','active')
    ->condition('fk_questionId',$questionId)
    ->execute()->fetchAll();    
    return $options;
}


function ws_get_school_solutions(){
    $solutions_data = prepare_solutiosn_data();
    return $solutions_data;
}