jQuery(document).ready(function(){
	 var l = window.location;
	 var base_url = l.protocol + "//" + l.host + "/";

	// Add Class
	jQuery('.inline-edit-hompage').click(function(){
		jQuery(this).addClass('editMode');
		jQuery(this).css({"background-color":"white","border":"1px solid #f1f1f1","padding":"5px 10px"});
		jQuery('.editMode span.glyphicon-pencil').addClass('hide');
	 });

	 // Save data
	jQuery(".inline-edit-hompage").focusout(function(){
		jQuery(this).removeClass("editMode");
		jQuery('.inline-edit-hompage span.glyphicon-pencil').removeClass('hide');
		jQuery(this).css({"background-color":"transparent","border":"none","padding":"0"});


		  var id = this.id;
		  var split_id = id.split("-");
		  var table_name = split_id[0];
		  var editId = split_id[1];
		  var value = jQuery(this).text();
		  value = value.trim();
	
		  jQuery.ajax({
			   url: '/admins/questions/order/save',
			   type: 'post',
			   data: { field:editId, value:value },
			   success:function(response){
				    console.log('Save successfully');
			   }
			  });
		 });
});


/** Clone Academic Year **/
function cloneAcademicYearModal(count){
	var cloneBtnId = "clone-btn-"+count; 
	var boardName = jQuery('#'+cloneBtnId).attr('data-board');
	var boardId = jQuery('#'+cloneBtnId).attr('data-boardId');
	var academicYear = jQuery('#'+cloneBtnId).attr('data-academicYear');
	var academicYearId = jQuery('#'+cloneBtnId).attr('data-academicYearId');
	var grade = jQuery('#'+cloneBtnId).attr('data-grade');
	var gradeId = jQuery('#'+cloneBtnId).attr('data-gradeId');
	
	jQuery('#board-name').val(boardName);
	jQuery('#academic-session').val(academicYear);
	jQuery('#grade-name').val(grade);
	
	jQuery('#board-id').attr('data-boardId',boardId);
	jQuery('#academicYear-id').attr('data-academicYearId',academicYearId);
	jQuery('#grade-id').attr('data-gradeId',gradeId);

}

function cloneAcademicYear(){
	var boardId = jQuery('#board-id').attr('data-boardId');
	var academicYearId = jQuery('#academicYear-id').attr('data-academicYearId');
	var gradeId = jQuery('#grade-id').attr('data-gradeId');
	var cloneAcademicSession = jQuery('#edit-clone-academicYear').val();
	
	jQuery
	.ajax({
		url : '/clone/academic/session',
		type : 'post',
		data : {
			cloneForSession : cloneAcademicSession,
			boardId: boardId,
			academicId: academicYearId,
			gradeId: gradeId
		},
		beforeSend : function() {
			jQuery('#modal-save-changes').attr("disabled", "disabled");
		},
		success : function(response) {
			alert("cloning");
			jQuery('#modal-message')
					.html('<strong>Success!</strong> Successfully made the clone.')
			jQuery('#modal-save-changes').hide();
			jQuery('#modal-message').show();
			location.reload();
		},
		error : function(error) {
			location.reload();
			//alert("Cloning Error");		
			jQuery('#modal-message')
					.html(
							'<strong>Failed!</strong> Unsuccessful in saving the changes. ')
			jQuery('#modal-save-changes').hide();
			jQuery('#modal-message').show();
		}
	});
}

