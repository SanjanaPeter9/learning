CREATE TABLE `school_boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `board_name` varchar(255) DEFAULT NULL COMMENT "Board or University name in case of courses",
  `type` enum('B','U') DEFAULT 'B' COMMENT "B=>Board, U=>University",
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `state` varchar(45) DEFAULT NULL,
  `allindia_board` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `school_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade` varchar(45) DEFAULT NULL,
  `alias_name` varchar(45) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;


CREATE TABLE `school_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `school_academic_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_year` int(11) DEFAULT NULL,
  `end_year` int(11) DEFAULT NULL,
  `academic_year` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `school_academic_chapters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_boardId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_boards',
  `fk_academicYearId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_academic_year',
  `fk_gradeId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_grade',
  `fk_subjectId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_subjects',  
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


/** Add Status in school_academic_year **/
Alter table school_academic_year add `status` enum('active','inactive') DEFAULT 'active';

CREATE TABLE `school_chapters_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_academicChapterId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_academic_chapters',
  `fk_chaptersId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_chapters',  
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `school_chapters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chapterName` varchar(512) NOT NULL COMMENT 'Chapter Name',
  `fk_gradeId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_grade',  
  `fk_subjectId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_subjects',  
  `type` enum('S','C') DEFAULT 'S' COMMENT "S=>School. C=>College",
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `school_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_academicChapterId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_academic_chapters',
  `fk_chapterId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_chapters',
  `fk_topicId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_chapter_topics',
  `question` varchar(512) NOT NULL COMMENT 'Question',
  `answer` text NOT NULL COMMENT 'Answer Content',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `school_question_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_academicChapterId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_academic_chapters',
  `title` varchar(256) DEFAULT NULL COMMENT 'Question Paper Title',
  `show_answer` enum('yes','no') DEFAULT 'yes' COMMENT 'Show Answers in Question Paper',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `school_questions_selected` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_questionPaperId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_academic_chapters',
  `fk_questionId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_chapters',
  `ordering` int(11) DEFAULT '0' COMMENT 'Order of the questions in question paper',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


/** Alter Commands **/

ALTER TABLE `school_academic_chapters` MODIFY `fk_boardId` int(11) NOT NULL COMMENT 'Primary Key from school_boards';
ALTER TABLE `school_academic_chapters` MODIFY `fk_academicYearId` int(11) NOT NULL COMMENT 'Primary Key from school_academic_year';
ALTER TABLE `school_academic_chapters` MODIFY `fk_gradeId` int(11) NOT NULL COMMENT 'Primary Key from school_grade';
ALTER TABLE `school_academic_chapters` MODIFY `fk_subjectId` int(11) NOT NULL COMMENT 'Primary Key from school_subjects';  
ALTER TABLE school_academic_chapters ADD FOREIGN KEY (fk_boardId) REFERENCES school_boards(id);
ALTER TABLE school_academic_chapters ADD FOREIGN KEY (fk_academicYearId) REFERENCES school_academic_year(id);
ALTER TABLE school_academic_chapters ADD FOREIGN KEY (fk_gradeId) REFERENCES school_grade(id);
ALTER TABLE school_academic_chapters ADD FOREIGN KEY (fk_subjectId) REFERENCES school_subjects(id);


ALTER TABLE `school_chapters_map` MODIFY `fk_academicChapterId` int(11) NOT NULL COMMENT 'Primary Key from school_academic_chapters';
ALTER TABLE `school_chapters_map` MODIFY `fk_chaptersId` int(11) NOT NULL COMMENT 'Primary Key from school_chapters';  
ALTER TABLE school_chapters_map ADD FOREIGN KEY (fk_academicChapterId) REFERENCES school_academic_chapters(id);
ALTER TABLE school_chapters_map ADD FOREIGN KEY (fk_chaptersId) REFERENCES school_chapters(id);


ALTER TABLE `school_chapters` MODIFY `fk_subjectId` int(11) NOT NULL COMMENT 'Primary Key from school_subjects';
ALTER TABLE school_chapters ADD FOREIGN KEY (fk_subjectId) REFERENCES school_subjects(id);


ALTER TABLE `school_questions` MODIFY `fk_academicChapterId` int(11) NOT NULL COMMENT 'Primary Key from school_academic_chapters';
ALTER TABLE `school_questions` MODIFY `fk_chapterId` int(11) NOT NULL COMMENT 'Primary Key from school_chapters';
ALTER TABLE school_questions ADD FOREIGN KEY (fk_academicChapterId) REFERENCES school_academic_chapters(id);
ALTER TABLE school_questions ADD FOREIGN KEY (fk_chapterId) REFERENCES school_chapters(id);


ALTER TABLE `school_question_paper` MODIFY `fk_academicChapterId` int(11) NOT NULL COMMENT 'Primary Key from school_academic_chapters';
ALTER TABLE school_question_paper ADD FOREIGN KEY (fk_academicChapterId) REFERENCES school_academic_chapters(id);


ALTER TABLE `school_questions_selected` MODIFY `fk_questionPaperId` int(11) NOT NULL COMMENT 'Primary Key from school_question_paper';
ALTER TABLE `school_questions_selected` MODIFY `fk_questionId` int(11) NOT NULL COMMENT 'Primary Key from school_questions';
ALTER TABLE school_questions_selected ADD FOREIGN KEY (fk_questionPaperId) REFERENCES school_question_paper(id);
ALTER TABLE school_questions_selected ADD FOREIGN KEY (fk_questionId) REFERENCES school_questions(id);


CREATE TABLE `school_chapter_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_chapterId` int(20) NOT NULL COMMENT 'Primary Key from school_academic_chapters',
  `topic` varchar(512) DEFAULT NULL COMMENT 'Topics Name',  
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `school_topics_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_chapterMapId` int(20) NOT NULL COMMENT 'Primary Key from school_chapters_map',
  `fk_topicId` int(20) NOT NULL COMMENT 'Primary Key from school_chapter_topics',  
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


ALTER TABLE school_boards ADD `type`  enum('B','U') DEFAULT 'B' COMMENT "B=>Board, U=>University";
ALTER TABLE school_academic_chapters MODIFY `fk_subjectId` int(11) DEFAULT NULL COMMENT 'Primary Key from school_subjects';


CREATE TABLE `school_question_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_questionId` int(11) NOT NULL COMMENT 'Primary Key from school_questions',
  `options` varchar(512) NOT NULL COMMENT 'Option value',  
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  FOREIGN KEY (fk_questionId) REFERENCES school_questions(id)  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


ALTER TABLE school_academic_chapters MODIFY `fk_subjectId` int(11) DEFAULT NULL COMMENT 'Primary Key from school_subjects';
ALTER TABLE school_questions ADD type enum('QnA','MCQ', 'FnB') COMMENT "QnA => Question & Answer, MCQ => Multiple Choice, FnB => Fill in the blanks";

ALTER TABLE school_question_paper ADD q_uniqueId varchar(128) COMMENT "Unique Question Paper Code";
ALTER TABLE school_questions MODIFY `question` text NOT NULL COMMENT 'Question';
ALTER TABLE  school_question_paper MODIFY `title` text DEFAULT NULL COMMENT 'Question Paper Title';


















/**** Colleges ****/
CREATE TABLE `college_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(512) NOT NULL COMMENT 'College Name',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `college_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseName` varchar(512) NOT NULL COMMENT 'Course Name',
  `fk_categoryId` int(11) NOT NULL COMMENT 'Category Id from college_categories',
  `type` enum('Full Time','Part Time','Distance') DEFAULT 'Full Time',
  `courseType` enum('Bachelor','Master','Phd') DEFAULT 'Bachelor', 
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  FOREIGN KEY (fk_categoryId) REFERENCES college_categories(id)  
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `college_terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_courseId` int(11) NOT NULL COMMENT 'Course Id from college_courses',  
  `term` enum('semester','yearly') DEFAULT 'semester',
  `termNumber,` int(11) NOT NULL COMMENT 'Term Number',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `college_term_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_courseId` int(11) NOT NULL COMMENT 'Course Id from college_courses',
  `fk_termId` int(11) NOT NULL COMMENT 'Term Id from college_course_terms',
  `fk_subjectId` int(11) NOT NULL COMMENT 'Term Id from college_term_subjects',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  FOREIGN KEY (fk_courseId) REFERENCES school_boards(id),
  FOREIGN KEY (fk_termId) REFERENCES college_terms(id),
  FOREIGN KEY (fk_subjectId) REFERENCES school_subjects(id)  
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;	


CREATE TABLE `college_term_subjects_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_courseId` int(11) NOT NULL COMMENT 'Course Id from college_courses',
  `fk_termId` int(11) NOT NULL COMMENT 'Term Id from college_course_terms',
  `fk_termSubjectId` int(11) NOT NULL COMMENT 'Term Id from college_term_subjects',
  `fk_chapterId` int(11) NOT NULL COMMENT 'Chapter Id from school_chapters',
  `question` varchar(512) NOT NULL COMMENT 'Question',
  `answer` text NOT NULL COMMENT 'Answer Content',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  FOREIGN KEY (fk_courseId) REFERENCES school_boards(id),
  FOREIGN KEY (fk_termId) REFERENCES college_terms(id),
  FOREIGN KEY (fk_termSubjectId) REFERENCES college_term_subjects(id),
  FOREIGN KEY (fk_chapterId) REFERENCES school_chapters(id)  
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;	


CREATE TABLE `college_course_question_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_universityId` int(11) NOT NULL COMMENT 'University Id from school_boards',
  'fk_academicYear' int(11) NOT NULL COMMENT 'Academic Year Id from school_academic_year',
  `fk_courseTermSubjectId` int(11) NOT NULL COMMENT 'Course Term Id from college_term_subjects',
  `title` varchar(256) DEFAULT NULL COMMENT 'Question Paper Title',
  `show_answer` enum('yes','no') DEFAULT 'yes' COMMENT 'Show Answers in Question Paper',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  FOREIGN KEY (fk_courseTermSubjectId) REFERENCES college_term_subjects(id)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `college_course_questions_selected` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_courseQuestionPaperId` int(11) NOT NULL COMMENT 'Course Question Paper Id from college_course_question_paper',
  `fk_courseQuestionId` int(11) NOT NULL COMMENT 'Question Id from college_term_subjects_questions',
  `ordering` int(11) DEFAULT '0' COMMENT 'Order of the questions in question paper',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  FOREIGN KEY (fk_courseQuestionPaperId) REFERENCES college_course_question_paper(id),
  FOREIGN KEY (fk_courseQuestionId) REFERENCES college_term_subjects_questions(id) 
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


School= BOARD, academic,  (unique question code)
Colleges = Course,Sem (unique question code)


Alter table school_boards Add `seo_string` varchar(255) DEFAULT NULL COMMENT "Seo string";
Alter table school_grade Add `seo_string` varchar(255) DEFAULT NULL COMMENT "Seo string";
Alter table school_subjects Add `seo_string` varchar(255) DEFAULT NULL COMMENT "Seo string";
Alter table school_chapters Add `seo_string` varchar(255) DEFAULT NULL COMMENT "Seo string";
Alter table school_chapter_topics Add `seo_string` varchar(255) DEFAULT NULL COMMENT "Seo string";


Alter table school_chapters Add `chapter_desc` text DEFAULT NULL COMMENT "Chapter description";
Alter table school_chapter_topics Add `topic_desc` text DEFAULT NULL COMMENT "Topic description";



/***21st January 2020 ****/
CREATE TABLE `seo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key:Seo ID.',
  `page_type` varchar(128) NOT NULL COMMENT 'Seo page type',
  `entity_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Ids from respective reference tables',
  `seo_source` varchar(512) NOT NULL COMMENT 'Seo url of Page, if at put only slug',
  `indexable` char(2) NOT NULL COMMENT 'Index->I, seoNo Index->NI',
  `anchor_text` varchar(256) DEFAULT NULL COMMENT 'Seo anchor text',
  `meta_page_title` varchar(512) DEFAULT NULL COMMENT 'Seo Meta Page Title',
  `meta_page_desc` varchar(1024) DEFAULT NULL COMMENT 'Seo Meta Description',
  `meta_keywords` varchar(512) DEFAULT NULL COMMENT 'Seo Meta Keywords',
  `created_at` datetime DEFAULT NULL COMMENT 'Created At',
  `updated_at` datetime DEFAULT NULL COMMENT 'Updated At',
  `status` enum('publish','draft','inactive') NOT NULL DEFAULT 'draft' COMMENT 'publish,draft,inactive',
  `redirection_url` varchar(512) DEFAULT NULL,
  `heading` varchar(256) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `description2` varchar(1024) DEFAULT NULL,
  `description3` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pageSeoSourceIndex` (`seo_source`(255))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Total data count against the page';


/*** 10th February 2020 ***/	
CREATE TABLE `school_solutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT "Board or University name in case of courses",
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `school_grade_subject_solutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_solutionId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_solutions',
  `fk_gradeId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_grade',
  `fk_subjectId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_subjects',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `school_solution_map_chapters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_mapSolutionId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_solutions',
  `fk_chapterId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_chapters',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `school_solution_map_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_mapSolutionId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_solutions',
  `fk_chapterId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_chapters',
  `fk_topicId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_topics',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `school_solution_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_solutionMapId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_academic_chapters',
  `fk_chapterId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_chapters',
  `fk_topicId` bigint(20) unsigned NOT NULL COMMENT 'Primary Key from school_chapter_topics',
  `question` varchar(512) NOT NULL COMMENT 'Question',
  `answer` text NOT NULL COMMENT 'Answer Content',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `school_solution_question_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_questionId` int(11) NOT NULL COMMENT 'Primary Key from school_questions',
  `options` varchar(512) NOT NULL COMMENT 'Option value',  
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  FOREIGN KEY (fk_questionId) REFERENCES school_questions(id)  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE school_solution_questions ADD type enum('QnA','MCQ', 'FnB') COMMENT "QnA => Question & Answer, MCQ => Multiple Choice, FnB => Fill in the blanks";
ALTER TABLE school_solution_questions MODIFY `question` text NOT NULL COMMENT 'Question';
ALTER TABLE school_solution_questions MODIFY `fk_solutionMapId` int(11) NOT NULL COMMENT 'Primary Key from school_grade_subject_solutions';
ALTER TABLE school_solution_questions MODIFY `fk_chapterId` int(11) NOT NULL COMMENT 'Primary Key from school_chapters';
ALTER TABLE school_solution_questions ADD FOREIGN KEY (fk_solutionMapId) REFERENCES school_grade_subject_solutions(id);
ALTER TABLE school_solution_questions ADD FOREIGN KEY (fk_chapterId) REFERENCES school_chapters(id);

ALTER TABLE seo MODIFY `description` text DEFAULT NULL;
ALTER TABLE seo MODIFY `description2` text DEFAULT NULL;
ALTER TABLE seo MODIFY `description3` text DEFAULT NULL;
ALTER TABLE seo ADD `fk_boardId` bigint(20) unsigned DEFAULT NULL COMMENT 'Primary Key from school_boards';
ALTER TABLE seo ADD `fk_gradeId` bigint(20) unsigned DEFAULT NULL COMMENT 'Primary Key from school_grade';



/** 13th April 2020 **/
ALTER TABLE school_grade MODIFY `grade` int(5) DEFAULT NULL;


/** 26th April 2020 **/
ALTER TABLE school_academic_year ADD `current_session` enum('yes','no') COMMENT "Current academic session" DEFAULT 'no';
